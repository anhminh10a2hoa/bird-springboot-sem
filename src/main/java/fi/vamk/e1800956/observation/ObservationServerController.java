package fi.vamk.e1800956.observation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600, allowCredentials = "true", methods = {RequestMethod.OPTIONS, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RestController
public class ObservationServerController {
  @Autowired
  private ObservationRepository repository;

  /** To save the observation to database we need a POST */
  @RequestMapping(value = "/observation" , method = RequestMethod.POST)
  public @ResponseBody
  Observation create(@RequestBody Observation obs) {
    return repository.save(obs);
  }
  
  @RequestMapping("/observation")
  public Iterable<Observation> getAllData(){
    return repository.findAll();
  }

  @RequestMapping("/observation/{id}")
  public Optional<Observation> get(@PathVariable("id") int id) {
    return repository.findById(id);
  }    

  @RequestMapping(value = "/observation" , method = RequestMethod.PUT)
  public @ResponseBody
  Observation update(@RequestBody Observation obs) {
    return repository.save(obs);
  } 
  
  @RequestMapping(value = "/observation" , method = RequestMethod.DELETE)
  public void delete(@RequestBody Observation obs) {
    repository.delete(obs);
  } 
}