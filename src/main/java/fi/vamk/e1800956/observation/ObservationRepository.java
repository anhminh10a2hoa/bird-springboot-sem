package fi.vamk.e1800956.observation;

import org.springframework.data.repository.CrudRepository;

public interface ObservationRepository extends CrudRepository<Observation, Integer> {

}