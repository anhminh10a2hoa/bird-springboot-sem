package fi.vamk.e1800956.bird;

import org.springframework.data.repository.CrudRepository;

public interface BirdRepository extends CrudRepository<Bird, Integer> {

}