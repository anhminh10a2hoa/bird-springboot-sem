package fi.vamk.e1800956.bird;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600, allowCredentials = "true", methods = {RequestMethod.OPTIONS, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RestController
public class BirdServerController {
  @Autowired
  private BirdRepository repository;

  /** To save the observation to database we need a POST */
  @RequestMapping(value = "/bird" , method = RequestMethod.POST)
  public @ResponseBody
  Bird create(@RequestBody Bird obs) {
    return repository.save(obs);
  }
  
  @RequestMapping("/bird")
  public Iterable<Bird> getAllData(){
    return repository.findAll();
  }

  @RequestMapping("/bird/{id}")
  public Optional<Bird> get(@PathVariable("id") int id) {
    return repository.findById(id);
  }    

  @RequestMapping(value = "/bird" , method = RequestMethod.PUT)
  public @ResponseBody
  Bird update(@RequestBody Bird obs) {
    return repository.save(obs);
  } 
  
  @RequestMapping(value = "/bird" , method = RequestMethod.DELETE)
  public void delete(@RequestBody Bird obs) {
    repository.delete(obs);
  } 
}