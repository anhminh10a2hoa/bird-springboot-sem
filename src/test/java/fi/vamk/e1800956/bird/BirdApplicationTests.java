package fi.vamk.e1800956.bird;

import fi.vamk.e1800956.observation.Observation;
import fi.vamk.e1800956.observation.ObservationRepository;
import fi.vamk.e1800956.observation.ObservationServerController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class BirdApplicationTests {

	@Autowired
	private ObservationServerController controller;

	@Autowired
	ObservationRepository repository;

	@Test
	void contextLoads() {
	}

	@Test
	public void checkThatControllerLoads() throws Exception {
		assertThat(controller).isNotNull();
	}

}
